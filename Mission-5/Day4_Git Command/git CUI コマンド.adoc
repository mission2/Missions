==== 基本コマンド
===== 通常の開発手順
https://qiita.com/sekiyaeiji/items/d0312c90bff4c37bc5dc[コマンド参考]
https://gist.github.com/yatemmma/6486028#file-git-lesson1-md[コマンド参考2]
----
1. ローカルの作業ディレクトリに移動する
$ cd 作業ディレクトリパス

※ローカルの作業ディレクトリが無ければ作成する
$ mkdir 新規フォルダ名

2. リモートリポジトリをローカルにクローンする
$ git clone リポジトリパス

3. ディレクトリ内の状態を調べる
$ ls

4. リポジトリディレクトリに移動する
$ cd リポジトリディレクトリ名

5. 全ブランチを一覧表示して確認する
$ git branch -a

6. ブランチを移動する
$ git checkout ブランチ名

7. ローカルカレントブランチmasterから新しいブランチを作成する
$ git checkout -b 新しいブランチ名

8. 開発する
$ code . ファイル名
-> VScodeでファイルを編集する

9. 開発ブランチの差分を確認する
$ git status

10. 追加・変更したファイルをすべてcommit候補に加える
$ git add *

追加・変更したファイルの一部をcommit候補に加える
$ git add 候補に加えたいファイル

11. 追加・変更したファイルをcommitメッセージ付きでcommitする
$ git commit -m "メッセージ"

12. リモートにpushする
$ git push origin ブランチ名

13. ブランチを移動する
$ git checkout ブランチ名

14. ローカルブランチを削除する
$ git branch -d ブランチ名
----
===== ローカルからリポジトリを立ち上げる
----
1. ローカルの作業ディレクトリに移動する
$ cd /作業ディレクトリ

2. リポジトリディレクトリを作成する
$ mkdir リポジトリディレクトリ名

3. ディレクトリ内の状態を調べる
$ ls

4. リポジトリディレクトリをgit管理ディレクトリに設定
$ git init

5. リモートリポジトリを追加する
$ git remote add origin リポジトリパス

6. ファイルを追加する

7. 追加したファイルをcommit候補に加える
$ git add *

8. 追加・変更したファイルをcommitメッセージ付きでcommit
$ git commit -m "コミットメッセージ"

9. リモートmasterブランチにpushする
$ git push origin master
----
===== 取り消すコマンド
----
addをとりけす
直近の1件のaddを取り消す
$ git reset HEAD

直近のcommitの変更点を確認する
$ git diff HEAD~1

直近のcommitを取り消す
$ git reset --soft HEAD^

取り消した状態のままforce pushする
$ git push origin ブランチ名 -f

commit履歴を確認する
$ git log

直近の3件分を取り消した場合の影響範囲を確認する
$ git diff HEAD~3

直近3件分のcommitを取り消す
$ git reset --soft HEAD~3

取り消した状態のままforce pushする
$ git push origin ブランチ名
----
===== commitの種類
----
$ git commit -m//メッセージ付きでコミット
$ git commit -a //変更のあったファイル全て
$ git commit --amend//直前のコミットを取り消す
$ git commit -v //変更点を表示してコミット
----
===== ブランチに関するコマンドTips
----
$git branch [branch_name]  //ブランチの作成
$git checkout [branch_name]  //ブランチの移動
$git branch -d [branch_name]  //ブランチの削除
$git branch -m [branch_name]  //現在のブランチ名の変更
$git branch // ローカルブランチの一覧
$git branch -a //リモートとローカルのブランチの一覧
$git branch -r //リモートブランチの一覧
$git checkout -b branch_name origin/branch_name //リモートブランチへチェックアウト
----
===== Master以外のブランチで編集した箇所をmasterに反映させる
----
$ git checkout ブランチ名
$ git commit -a -m "コメント"//変更ファイルをコミット
$ git checkout master
$ git merge ブランチ名//差分をマージ
$ git push origin master//ファイルの行進

※マージを取り消す
$ git merge --abort
----
===== 削除
----
ファイルの削除
$ git rm ファイル名 //特定のファイルorディレクトリの削除
$ git rm * //全ファイルorディレクトリ
$ git commit -a -m "remove"//削除をコミット
$ chgit push origin master //削除を反映
----
===== pull
----
$ git pull origin ブランチ名
-> カレントローカルブランチに、指定したリモートのブランチをpullする
----
mv	[ファイル名]を[ディレクトリ]へ移動する	mv style.css assets/css
cp	[ファイル1]を[ファイル2]という名前でコピーする	cp index.html about.html