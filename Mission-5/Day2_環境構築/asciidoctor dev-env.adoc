== ER図をAsciiDocで書く方法
. asciidoctor-diagramを使い、plantUMLで作られた図を埋め込まれた.adocを読み込む
. 環境構築
.. ruby 開発環境
.. java ランタイム
.. graphviz
.. asciidoctor
* asciidoctor-diagram
* asciidoctor-pdf
* asciidoctor-pdf-cjk(いらないかも)

==== Asciidoctor
----

Asciidoctorを使用するには、
1. ruby環境の構築(MyCentOS内で構築済み)
2. ruby gem から、asciidoctor, asciidoctor-pdf, asciidoctor-diagramインストール
$ gem install asciidoctor asciidoctor-pdf asciidoctor-diagram
3. yumを使用して、Graphviz, javaをインストール
$ sudo yum install java-1.8.0-openjdk
$ sudo yum install graphviz

HTML変換
asciidoctor test.adoc
=> test.htmlが作成される

PDFファイル変換
asciidoctor-pdf test.adoc
=> test.pdfが作成される

複数ファイルまとめて変換
asciidoctor *.adoc
asciidoctor-pdf *.adoc

asciidoctor-pdf-cjk や asciidoctor-diagramを使う場合(-rをつける)
asciidoctor     -r asciidoctor-pdf-cjk -r asciidoctor-diagram test.adoc
asciidoctor-pdf -r asciidoctor-pdf-cjk -r asciidoctor-diagram test.adoc

シンタックスハイライト
コードをシンタックスハイライトするためのプラグイン
gem install coderay
----
https://qiita.com/tamikura@github/items/5d3f62dae55617ee42bb[インストール手順参考]

==== Asciidoc Diagram
----
Asciidoc Diagram はいろいろな図形出力に対応した Asciidoc 拡張
[plantuml] をつけたブロックを書き、asciidoctor-diagram を拡張として読み込むだけで利用することができる

sample.adoc
[plantuml]
----
class Animal {
  run()
}

class Cat extends Animal {
}
----
