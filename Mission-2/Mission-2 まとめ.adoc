== Day 5-6でやったこと 
== Mission2:「git」と「github flow」によるコード管理を会得する 

=== 要件 
* WEBのフリーサービスのgitlabを使う 
* ローカルPC上でも使えるようにする 
* GUIでなくCUIでも操作できるようになる 
* 学んだことを、同じようなレベルの人に教えられるようにまとめる(asciidoc、図表付き） 
 
=== 要件分析

 要件の単語の意味確認
 役割分担を行い、要件に記載されている単語の中でわからないものを検索 
 広瀬：git, github flow, gitlab 
 手取：GUI/CUI,ローカルで使用可能の意味 
 
 作業列挙・工数見積もり・2日間のプラン立て 
 作業を列挙し、2日間で要件を満たすためのプランを立てる 
 要件の要素を下記1～3に分解し、工数を見積もった

. 環境構築（3h） 
.. git, git bash, VSC, gitlab, github flowの理解を深め、インストールしたシステムで何ができるのか確認 
.. 要件との適合性を確認（ローカルPC上で使えるか、GUI/CUI両方で使えるのか） 
.. Repositoryの作成(local & remote) 
. 使用法習得(4h) 
. 次の人のために記録と改善版手順をまとめる(4h) 
.. 図表つきのAscii docスタイルでまとめる 
.. 担当を決めて効率よく行う 
 
===== 良かった点 
* ペアで作業をすることで、2人分のトライ＆エラーが行えたため、設定を行いやすかった。 
  
===== 悪かった点 
* 作業を分担し、共有を繰り返して効率よく進められれば良かったが、前提となる知識がお互いに不足していたため結局理解に時間がかかってしまった。 
* 参照するサイトを開きすぎ、どのサイトが有用であるのか、どのサイトを閲覧していたのかがわからなくなり、作業に響いた。はてブを効率よく使えるようになる必要がある。 
* gitを使うことで実現しようとしていることは概ね理解できたが、そのためのツールがそれぞれどんな働きをしているのか理解するのが困難であり工数見積もりに響いた。 
 
=== 環境構築 
. gitlabのアカウント作成(gitlab.com) 
.. アカウント作成 
.. link:https://qiita.com/masakura/items/29f8949379fa86dac22c[gitlabとは] 
. git for windowsインストール
.. link:https://opcdiary.net/?page_id=27065[インストール手順] 
.. 特に迷った点 
* どのeditorを選択するか 
* ->デフォルトであるvimの使用を公式が推奨しておらず、残りの選択肢の中で調べた限り汎用性が高いと感じたVisual Studio Codeをインストールする事にした。 
* link:https://techacademy.jp/magazine/10569[VSCを選んだ理由] 
* link:https://eng-entrance.com/texteditor-vscode[インストール手順] 
** VSCをインストールして選択しても、”Next”が押せない 
** ->インストール後に、gitインストール内でVSCをクリックして”Back”するとクリックenableになる。 
 
. SSH keyの作成 
.. Git Bash を使ってユーザー名とメアドを登録 
.. SSHkeyを作成 
.. https://qiita.com/evansplus/items/08cf1284ed8d5837c9cf[参考url] 
.. GitlabへSSHkey(public)を登録 
.. 接続確認 
* error発生、解決に時間がかかった
* errorに関しては後述 

. Gitクライアントのセットアップ(GUI) 
.. SourceTreeのinstall 
* <-使用者が多く日本語にも対応し、情報が得やすい 
.. SourceTreeにSSHkey(private)を登録 
.. ローカルリポジトリ作成 
* 空のファイルを使用 
.. リモートリポジトリの設定(Gitlab) 
* エラー発生 
 
==== 悪かった点 
* VSCでGUIのGitクライアントとして使おうと思っていたが、うまくいかなかったこと 
* editorは今回のミッションにおいて重要ではなかったにも関わらず、その点に気付くことができず、時間を消費した。 
* Port 22に接続できない設定になっているということに気付くのに非常に時間がかかった。別のSSHkeyを使用するなど見当違いのことをしてしまい、結果として時間を無駄にした。 
 
 
==== 良かった点 
* 情報を調べすぎるのではなく、ある程度集まった段階で実際にインストールして試すことで理解するプロセスを踏んだのは良かった。 
* 明確な理由付けをしたうえでinstallする対象を決定することができた。 
 
 
=== エラーリスト 
[options="header"] 
|=================================================== 
|エラー			|タイミング				|解決策 
|port 22 connection error	|Gitlabとの接続確認時		| port 443を使用 #1,#2 
|portに関する変更の保存	|Gitlabとの接続確認時		|~/.sshにconfigを作成 #3 
| SSHkey認証エラー	|SourceTreeとGitlabの接続時	| SSHkey(private)を登録or port443に接続する設定 #4 
|===================================================
  
* link:https://qiita.com/pinemz/items/4be98d4ff446e7fd023b[#1] 
* link:https://qiita.com/SOJO/items/74b16221580a17296226[#2]
* link:https://qiita.com/0084ken/items/2e4e9ae44ec5e01328f1[#3]

* link:https://azunobu.hatenablog.com/entry/2015/08/06/201449[#4]
 
=== 使用法習得 
* Source tree(GUI)を用いて、github flowに沿ったgitによるバージョン管理を会得 
(CUIに関しては、コマンドを確認しながらの作業であれば可能) 
* github flowの6つのルール 
. masterブランチは常にリリース可能な状態にしておく 
. かならずmasterブランチからtopicブランチを切る 
. ブランチは定期的にPushする 
. プルリクエストを使ってコードレビューをする 
. masterブランチにマージする前にコードレビューを行う 
. コードレビューを通過したらすみやかにマージする 
 
以上のルールを踏まえた上で、以下各自で作業

* link:http://tracpath.com/bootcamp/learning_git_github_flow.html[github flow活用] 
* Remote repositoryをgitlab上に作成(初期設定でmasterが作成される) 
* Local repositoryを個人フォルダに作成（フォルダの中身は空でなければならない） 
* Remote repositoryをLocal repositoryにcloneする 
* 作業用のbranchを作成 (localもremoteもmasterには一切触れてはいけない) 
* 作業用branch内にファイルを追加 
* add, commitを行い、remote repositoryへpush (local上でmasterにはmergeしない) 
* gitlab上でmerge requestを送る 
* Approveされたら、localとremoteの両方から、該当のbranchを削除する 
* Localのmasterが古いままなので、pullを使用して更新する。 
上記を繰り返していくのが、github flow　 

* link:https://www.sejuku.net/blog/5756[gitとは] 
 
===== 良かった点 
* 概念図をかけるレベルまで理解できた。 
* 実際に、remote repositoryとlocal repositoryを連携して作業できるまで理解できた。 
* 概念や機能を理解する上でGUIから取り組んだのは良かった。 
* 始める前に意思確認ができていたため、目的をもって作業する事ができた。 
 
===== 悪かった点 
* 見た目にわかりやすいGUIに時間を費やしたため、GUIに比べるとCUIの費やす時間が少なくなった。 
 
== これまでの大きな問題点と改善のアクション 
==== ウェブサイトの閲覧 
* 有用なサイトや度々閲覧するサイトはウィンドウを分け、差別化する。はてブに専用のタグを設け、差別化する。 

==== 要件達成への意識 
* 要件を常に意識して、目的を持って調べるなり作業するなりする。 
* ->今回のVSCインストールのように、直接要件達成に関係のないものに時間をかけすぎない。

==== 役割分担がうまく機能していない 
* 作業を効率的に進ませるために、取り組むべき作業を細分化する。 
* 目的を持ち、担当している作業が時間内に終わらないようであれば早めに報告しあう。 

==== 工数見積もり 
* インストールの作業にかかる時間を見抜けなかった。 
* 要件分析の時点で、開発環境を整える必要があるかを見抜き、必要な場合は時間に余裕をもってプランを立てる。 